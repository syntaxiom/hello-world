# Hi there!

Nice to meet you! :wolf: How are you?

[Click here to dive deeper!](1-deep)

```werewolf-test
print("Hello!")
```

```js
console.log("Just some JavaScript! 😎");
```

```werewolf-test
print("World!")
```

```werewolf-directory
> rootbeer
>> src
>>> app.js
>>> README.md
>>> private
>>>> secrets.json
>>>> definitely-legal.zip
>>> index.html
>> assets
>>> Tibbles.png
>>> robots.txt
>>> styles.css
>> hello.docx
```

```werewolf-panel
Clickbait title?!

Long ago, the <b>4 nations</b> lived together in harmony...
```

[//]: # (This may be the most platform independent comment - https://stackoverflow.com/a/20885980)

My list:

* One
* Two
* Three!