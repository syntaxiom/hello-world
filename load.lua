data.dx = 0
data.dy = 0

data.blinkOn = true

function distance(x1, y1, x2, y2)
    local a = x1 - x2
    local b = y1 - y2
    local c = math.sqrt(a^2 + b^2)
    return c
end