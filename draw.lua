-- -- Teach people to optimize by NOT putting functions in "draw.lua"
function greet(name)
  return "Hi, " .. name
end

-- cursor.loose = true
cursor.y = 50
-- font.underline = true
print("Hello, world!")

cursor.x = 375 + data.dx
font.italic = true
-- font.underline = false
print(greet("Wolfy"))

-- cursor.loose = false
cursor.y = 0
font.bold = true
print("Osborn\nOscar", "Oswald")

-- for i=1,10 do
--     cursor.x = cursor.x + data.dx
--     color.background = "red"
--     print("\n\n\n   ")
-- end

-- color.background = "white"
cursor.visible = data.blinkOn
print("█  ██\n██  █\n███")

data.dx = data.dx + 1